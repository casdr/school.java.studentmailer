package me.casdr.java.studentmailer;

/**
 * Created by casdr on 19/09/2016.
 */
public class Student {

    /**
     * Name of the student
     */
    public String name,
            /**
             * E-mail address of the student
             */
            emailAddress,
            /**
             *
             */
            group;

    /**
     * Create new student object
     *
     * @param columns
     */
    public Student(String[] columns) {
        name = columns[0] + " " + columns[1];
        emailAddress = columns[2];
        group = columns[3];
    }
}
