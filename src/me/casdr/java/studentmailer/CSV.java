package me.casdr.java.studentmailer;

/**
 * Created by casdr on 19/09/2016.
 */
import javax.swing.*;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author casdr
 */
public class CSV {

    /**
     * Lines in the CSV file
     */
    public List<String[]> lines = new ArrayList<String[]>();

    /**
     * Get a CSV
     *
     * @param csvUrl
     */
    public CSV(String csvUrl) {
        String file = "";
        try {
            URL url = new URL(csvUrl);
            Scanner s = new Scanner(url.openStream());
            while (s.hasNext()) {
                file += s.nextLine() + "\n";
            }
            s.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Kon de CSV niet ophalen", "Fout opgetreden", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        String csvSplitBy = ",";
        String tLines[] = file.split("\\r?\\n");

        for (String line : tLines) {
            String[] columns = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
            lines.add(columns);
        }
    }
}
