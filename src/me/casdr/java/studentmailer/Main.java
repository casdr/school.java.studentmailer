package me.casdr.java.studentmailer;

/**
 * Created by casdr on 19/09/2016.
 */
import java.awt.*;
import java.net.URI;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author casdr
 */
public class Main {

    /**
     * Run the application
     *
     * @param args
     */
    public static void main(String[] args) {

        // Get the CSV
        CSV csv = new CSV("http://cas.yt/random/maillijst.txt");
        Map<String, Group> groups = new HashMap<String, Group>();

        for (String[] columns : csv.lines) {
            Student student = new Student(columns);
            Group group = groups.get(student.group);
            if (group == null) {
                groups.put(student.group, new Group(student.group));
                group = groups.get(student.group);
            }
            group.students.add(student);
        }
        String selected = (String) JOptionPane.showInputDialog(null,
                "Selecteer een groep",
                "Selecteer een groep",
                JOptionPane.QUESTION_MESSAGE,
                null,
                groups.keySet().toArray(),
                "2IAO3A"
        );
        Group group = groups.get(selected);
        String[] studentArray = new String[group.students.size()];
        int studentI = 0;
        for (Student student : group.students) {
            studentArray[studentI] = String.format("%s", student.name);
            studentI++;
        }
        JList studentList = new JList(studentArray);
        studentList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        studentList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        int end = studentList.getModel().getSize() - 1;
        if (end >= 0) {
            studentList.setSelectionInterval(0, end);
        }
        JOptionPane.showMessageDialog(
                null,
                studentList,
                "Selecteer de studenten",
                JOptionPane.PLAIN_MESSAGE
        );
        String emailList = "";
        for (Integer studentId : studentList.getSelectedIndices()) {
            Student student = group.students.get(studentId);
            emailList += student.emailAddress + ",";
        }
        emailList = emailList.replaceAll(",$", "");
        try {
            Desktop.getDesktop().mail(new URI("mailto:" + emailList));
            System.exit(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Kon het mailprogramma niet openen.", "Fout opgetreden", JOptionPane.ERROR_MESSAGE);
        }
    }
}
