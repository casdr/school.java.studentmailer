package me.casdr.java.studentmailer;

/**
 * Created by casdr on 19/09/2016.
 */
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author casdr
 */
public class Group {

    /**
     * Group code
     */
    public String name;

    /**
     * List of students
     */
    public List<Student> students = new ArrayList<Student>();

    /**
     * Constructor
     *
     * @param groupName Group code
     */
    public Group(String groupName) {
        name = groupName;
    }
}
